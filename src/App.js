import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  	
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Hate Speech Analyzer with React</h1>
        </header>
		<div>
            <textarea type = "text" placeholder = "Ketik Tweet (max: 250 karakter)" 
               onChange = {this.updateState} rows="10" cols="30" maxlength="250"/>
			   <br/>
            <button class="form-control" type="submit">Submit</button>
         </div>
      </div>
    );
  }
}

export default App;
